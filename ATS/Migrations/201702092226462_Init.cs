namespace ATS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemTypes",
                c => new
                    {
                        ItemTypeID = c.Int(nullable: false, identity: true),
                        ItemTypeName = c.String(),
                        PricePerUnit = c.Single(nullable: false),
                        EffectiveDate = c.DateTime(nullable: false),
                        ServiceDescription = c.String(),
                        ItemCode = c.String(),
                        ItemDescription = c.String(),
                        FormName = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        FormFamily_TaxFormFamilyID = c.Int(),
                        Origin_TaxOriginID = c.Int(),
                    })
                .PrimaryKey(t => t.ItemTypeID)
                .ForeignKey("dbo.TaxFormFamilies", t => t.FormFamily_TaxFormFamilyID)
                .ForeignKey("dbo.TaxOrigins", t => t.Origin_TaxOriginID)
                .Index(t => t.FormFamily_TaxFormFamilyID)
                .Index(t => t.Origin_TaxOriginID);
            
            CreateTable(
                "dbo.DigitalServiceTechnologies",
                c => new
                    {
                        DigitalServiceTechnologyID = c.Int(nullable: false, identity: true),
                        TechnologyName = c.String(),
                        TechnologyAbbrev = c.String(),
                        TechnologyDescription = c.String(),
                        DigitalServiceTechnology_DigitalServiceTechnologyID = c.Int(),
                        DigitalServiceItem_ItemTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.DigitalServiceTechnologyID)
                .ForeignKey("dbo.DigitalServiceTechnologies", t => t.DigitalServiceTechnology_DigitalServiceTechnologyID)
                .ForeignKey("dbo.ItemTypes", t => t.DigitalServiceItem_ItemTypeID)
                .Index(t => t.DigitalServiceTechnology_DigitalServiceTechnologyID)
                .Index(t => t.DigitalServiceItem_ItemTypeID);
            
            CreateTable(
                "dbo.ItemUnits",
                c => new
                    {
                        ItemUnitID = c.Int(nullable: false, identity: true),
                        ItemUnitName = c.String(),
                        ItemUnitAbbrev = c.String(),
                    })
                .PrimaryKey(t => t.ItemUnitID);
            
            CreateTable(
                "dbo.TaxFormFamilies",
                c => new
                    {
                        TaxFormFamilyID = c.Int(nullable: false, identity: true),
                        TaxFormFamilyName = c.String(),
                        TaxFormFamilyAbbrev = c.String(),
                    })
                .PrimaryKey(t => t.TaxFormFamilyID);
            
            CreateTable(
                "dbo.TaxOrigins",
                c => new
                    {
                        TaxOriginID = c.Int(nullable: false, identity: true),
                        TaxOriginName = c.String(),
                        TaxOriginAbbrev = c.String(),
                    })
                .PrimaryKey(t => t.TaxOriginID);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        DiscountID = c.Int(nullable: false, identity: true),
                        EffectiveDate = c.DateTime(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        PublicationName = c.String(),
                        PublicationDate = c.DateTime(),
                        UnitType_DiscountUnitID = c.Int(),
                    })
                .PrimaryKey(t => t.DiscountID)
                .ForeignKey("dbo.DiscountUnits", t => t.UnitType_DiscountUnitID)
                .Index(t => t.UnitType_DiscountUnitID);
            
            CreateTable(
                "dbo.DiscountUnits",
                c => new
                    {
                        DiscountUnitID = c.Int(nullable: false, identity: true),
                        DiscountUnitName = c.String(),
                        DiscountUnitAbbrev = c.String(),
                        DiscountUnitDesc = c.String(),
                    })
                .PrimaryKey(t => t.DiscountUnitID);
            
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        LicenseID = c.Int(nullable: false, identity: true),
                        Licensee_StaffMemberID = c.Int(),
                        Type_LicenseTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.LicenseID)
                .ForeignKey("dbo.StaffMembers", t => t.Licensee_StaffMemberID)
                .ForeignKey("dbo.LicenseTypes", t => t.Type_LicenseTypeID)
                .Index(t => t.Licensee_StaffMemberID)
                .Index(t => t.Type_LicenseTypeID);
            
            CreateTable(
                "dbo.StaffMembers",
                c => new
                    {
                        StaffMemberID = c.Int(nullable: false, identity: true),
                        StaffMemberName = c.String(),
                        StaffMemberImageLink = c.String(),
                        StaffMemberBio = c.String(),
                        AccountOwner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.StaffMemberID)
                .ForeignKey("dbo.AspNetUsers", t => t.AccountOwner_Id)
                .Index(t => t.AccountOwner_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        SecurityQuestion = c.String(nullable: false, maxLength: 255),
                        SecurityAnswer = c.String(nullable: false, maxLength: 255),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.LicenseTypes",
                c => new
                    {
                        LicenseTypeID = c.Int(nullable: false, identity: true),
                        LicenseTypeName = c.String(),
                        LicenseTypeNumberAddendum = c.String(),
                        LicenseTypeAbbrev = c.String(),
                        LicenseTypeDescription = c.String(),
                    })
                .PrimaryKey(t => t.LicenseTypeID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.ItemTypeItemUnits",
                c => new
                    {
                        ItemType_ItemTypeID = c.Int(nullable: false),
                        ItemUnit_ItemUnitID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ItemType_ItemTypeID, t.ItemUnit_ItemUnitID })
                .ForeignKey("dbo.ItemTypes", t => t.ItemType_ItemTypeID, cascadeDelete: true)
                .ForeignKey("dbo.ItemUnits", t => t.ItemUnit_ItemUnitID, cascadeDelete: true)
                .Index(t => t.ItemType_ItemTypeID)
                .Index(t => t.ItemUnit_ItemUnitID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Licenses", "Type_LicenseTypeID", "dbo.LicenseTypes");
            DropForeignKey("dbo.Licenses", "Licensee_StaffMemberID", "dbo.StaffMembers");
            DropForeignKey("dbo.StaffMembers", "AccountOwner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Discounts", "UnitType_DiscountUnitID", "dbo.DiscountUnits");
            DropForeignKey("dbo.ItemTypes", "Origin_TaxOriginID", "dbo.TaxOrigins");
            DropForeignKey("dbo.ItemTypes", "FormFamily_TaxFormFamilyID", "dbo.TaxFormFamilies");
            DropForeignKey("dbo.ItemTypeItemUnits", "ItemUnit_ItemUnitID", "dbo.ItemUnits");
            DropForeignKey("dbo.ItemTypeItemUnits", "ItemType_ItemTypeID", "dbo.ItemTypes");
            DropForeignKey("dbo.DigitalServiceTechnologies", "DigitalServiceItem_ItemTypeID", "dbo.ItemTypes");
            DropForeignKey("dbo.DigitalServiceTechnologies", "DigitalServiceTechnology_DigitalServiceTechnologyID", "dbo.DigitalServiceTechnologies");
            DropIndex("dbo.ItemTypeItemUnits", new[] { "ItemUnit_ItemUnitID" });
            DropIndex("dbo.ItemTypeItemUnits", new[] { "ItemType_ItemTypeID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.StaffMembers", new[] { "AccountOwner_Id" });
            DropIndex("dbo.Licenses", new[] { "Type_LicenseTypeID" });
            DropIndex("dbo.Licenses", new[] { "Licensee_StaffMemberID" });
            DropIndex("dbo.Discounts", new[] { "UnitType_DiscountUnitID" });
            DropIndex("dbo.DigitalServiceTechnologies", new[] { "DigitalServiceItem_ItemTypeID" });
            DropIndex("dbo.DigitalServiceTechnologies", new[] { "DigitalServiceTechnology_DigitalServiceTechnologyID" });
            DropIndex("dbo.ItemTypes", new[] { "Origin_TaxOriginID" });
            DropIndex("dbo.ItemTypes", new[] { "FormFamily_TaxFormFamilyID" });
            DropTable("dbo.ItemTypeItemUnits");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.LicenseTypes");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.StaffMembers");
            DropTable("dbo.Licenses");
            DropTable("dbo.DiscountUnits");
            DropTable("dbo.Discounts");
            DropTable("dbo.TaxOrigins");
            DropTable("dbo.TaxFormFamilies");
            DropTable("dbo.ItemUnits");
            DropTable("dbo.DigitalServiceTechnologies");
            DropTable("dbo.ItemTypes");
        }
    }
}
