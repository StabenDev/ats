﻿///<reference path="./../typings/globals/core-js/index.d.ts"/>
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// Custom Components
import { AppComponent } from './app.component';

// Route Modules
import { HomeComponent } from './SiteData/Home/home.component';
import { PricingComponent } from './SiteData/Home/pricing.component';
import { NewsComponent } from './SiteData/Home/news.component';
import { FormComponent } from './SiteData/Home/form.component';
import { StaffComponent } from './SiteData/Home/staff.component';
import { ContactComponent } from './SiteData/Home/contact.component';
import { RouteNotFoundComponent } from './SiteData/Error/RouteNotFound.component';
// Routes
import { RoutingRoot } from './app.routing';

@NgModule({
    imports: [
        BrowserModule,
        RoutingRoot
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        PricingComponent,
        NewsComponent,
        FormComponent,
        StaffComponent,
        ContactComponent,
        RouteNotFoundComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }