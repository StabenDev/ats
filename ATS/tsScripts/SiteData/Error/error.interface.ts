﻿export interface ErrorTypeInterface {
    ErrorCode: number;
    ErrorMessage: String;
}