﻿import { Component } from '@angular/core';
import { ErrorTypeInterface } from './error.interface';

// Templates go in the Scripts folder or it won't pub correctly!

@Component({
    templateUrl: 'Scripts/SiteData/error.template.html'
})

export class RouteNotFoundComponent implements ErrorTypeInterface {

    ErrorCode = 404;
    ErrorMessage = "Page not Found.";
    
}