﻿// Angular Components
import { Router, RouterModule } from '@angular/router';

// Custom Components
import { HomeComponent } from './SiteData/Home/home.component';
import { PricingComponent } from './SiteData/Home/pricing.component';
import { NewsComponent } from './SiteData/Home/news.component';
import { FormComponent } from './SiteData/Home/form.component';
import { StaffComponent } from './SiteData/Home/staff.component';
import { ContactComponent } from './SiteData/Home/contact.component';
import { RouteNotFoundComponent } from './SiteData/Error/RouteNotFound.component';

export const RoutingRoot = RouterModule.forRoot([
    { path: '', component: HomeComponent },
   
    { path: 'pricing', component: PricingComponent },
    { path: 'news', component: NewsComponent },
    { path: 'form', component: FormComponent },
    { path: 'staff', component: StaffComponent },
    { path: 'contact', component: ContactComponent },
    { path: '**', component: RouteNotFoundComponent /* need a 404 component later */ }
  
]);