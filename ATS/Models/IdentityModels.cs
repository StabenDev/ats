﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace ATS.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual StaffMember StaffAccountData { get; set; }

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(255)]
        public string SecurityQuestion { get; set; }

        [Required]
        [MaxLength(255)]
        public string SecurityAnswer { get; set; }

        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            // Since we are using a strict API we will shut down lazy loading by default.
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<ApplicationUser>()
                .HasOptional<StaffMember>(s => s.StaffAccountData)
                .WithOptionalPrincipal(a => a.AccountOwner);
            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<StaffMember> Staff { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<LicenseType> LicenseTypes { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<DiscountUnit> DiscountUnits { get; set; }
        public DbSet<PhysicalItem> PhysicalItems { get; set; }
        public DbSet<DigitalServiceItem> DigitalServiceItems { get; set; }
        public DbSet<DigitalServiceTechnology> DigitalServiceTechnologies { get; set; }
        public DbSet<TaxItem> TaxItems { get; set; }
        public DbSet<TaxFormFamily> TaxFormFamilies { get; set; }
        public DbSet<TaxOrigin> TaxOrigins { get; set; }
        public DbSet<ItemUnit> ItemUnits { get; set; }
    }
}