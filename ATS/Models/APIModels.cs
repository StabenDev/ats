﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ATS.Models
{
    public class StaffMember
    {
        public int StaffMemberID { get; set; }
        public String StaffMemberName { get; set; }
        public String StaffMemberImageLink { get; set; }
        public String StaffMemberBio { get; set; }
        public virtual ICollection<License> Licesnses { get; set; }

        // ApplicationUser data should NEVER be serialized for the WebAPI
        [JsonIgnore]
        public virtual ApplicationUser AccountOwner { get; set; }
    }

    public class License
    {
        public int LicenseID { get; set; }
        public virtual LicenseType Type { get; set; }
        [JsonIgnore]
        public virtual StaffMember Licensee { get; set; }
    }

    public class LicenseType
    {
        public int LicenseTypeID { get; set; }
        public String LicenseTypeName { get; set; }
        public String LicenseTypeNumberAddendum { get; set; }
        public String LicenseTypeAbbrev { get; set; }
        public String LicenseTypeDescription { get; set; }
        [JsonIgnore] // <== This will cause the API to ignore the variable, do this for any cyclical links so we don't get an infinite loop!
        public virtual ICollection<License> LicensesOfType { get; set; }
    }

    public abstract class ItemType
    {
        public int ItemTypeID { get; set; }
        public String ItemTypeName { get; set; }
        public virtual ICollection<ItemUnit> UnitType { get; set; }
        public float PricePerUnit { get; set; }
        public DateTime EffectiveDate { get; set; } 
        
    }

    public abstract class Discount
    {
        public int DiscountID { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public virtual DiscountUnit UnitType { get; set; }
    }

    public class PublicationDiscount : Discount
    {
        public string PublicationName { get; set; }
        public DateTime PublicationDate { get; set; }
    }

    public class DiscountUnit
    {
        public int DiscountUnitID { get; set; }
        public String DiscountUnitName { get; set; }
        public String DiscountUnitAbbrev { get; set; }
        public String DiscountUnitDesc { get; set; }
        public virtual ICollection<Discount> DiscountsOfType { get; set; }
    }

    public class PhysicalItem : ItemType
    {
        public String ItemCode { get; set; }
        public String ItemDescription { get; set; }
    }

    public class DigitalServiceItem : ItemType
    {
        public virtual ICollection<DigitalServiceTechnology> Technologies { get; set; }  
        public String ServiceDescription { get; set; }
    }

    public class DigitalServiceTechnology
    {
        public int DigitalServiceTechnologyID { get; set; }
        public String TechnologyName { get; set; }
        public String TechnologyAbbrev { get; set; }
        public String TechnologyDescription { get; set; }
        public virtual ICollection<DigitalServiceTechnology> ServicesUsedBy { get; set; }
    }

    public class TaxItem : ItemType 
    {
        public virtual TaxOrigin Origin { get; set; } 
        public String FormName { get; set; }
        public virtual TaxFormFamily FormFamily { get; set; }
    }

    public class TaxFormFamily // tax form types, and siblings: form type 1040 goes to 1040EX ect.
    {
        public int TaxFormFamilyID { get; set; }
        public String TaxFormFamilyName { get; set; }
        public String TaxFormFamilyAbbrev { get; set; }
        public virtual ICollection<TaxItem> ItemsInFamily { get; set; }
    }
    public class TaxOrigin
    {
        public int TaxOriginID { get; set; }
        public String TaxOriginName { get; set; }
        public String TaxOriginAbbrev { get; set; }
        public virtual ICollection<TaxItem> TaxItems { get; set; }
    }

    public class ItemUnit
    {
        public int ItemUnitID { get; set; }
        public String ItemUnitName { get; set; }
        public String ItemUnitAbbrev { get; set; }
        public virtual ICollection<ItemType> ItemTypesOfUnit { get; set; }
    }
}