﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ATS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Map ACP routes to pure Server Control
            routes
                .MapRoute(
                    name: "Admin",
                    url: "Admin/{action}/{data}",
                    defaults: new { controller = "Admin", action = "Login" }
                );
            // Send all Non-API/Admin directives to the AngularJS-2 Front end
            // Preserves URI for AngularJS-2 to handle errors.
            routes.MapRoute(
                name: "Default",
                url: "{*anything}",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}