﻿using ATS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace ATS.Controllers
{
    [RoutePrefix("api/Staff")]
    public class StaffController : ApiController
    {

        // This is the correct controller to model after

        /// <summary>
        /// Get a specific staff member by their registered ID number
        /// </summary>
        /// <param name="id">ID number of the staff person</param>
        /// <returns>StaffMember</returns>
        [Route("ID/{id}")]
        [HttpGet]
        public object GetStaffMember(string id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                // Try Catch due to converting a string to a number, this CAN and DOES go wrong!
                try
                {
                    // Parse out the ID to an integer, I do not trust parameter constraints in .NET
                    int ID;
                    Int32.TryParse(id,out ID);
                    // Grab the staff person from the database if it exists
                    StaffMember StaffPerson = db.Staff.FirstOrDefault(c => c.StaffMemberID == ID);

                    /*
                     * Raw Test Object
                     * 
                    StaffMember StaffPerson = new StaffMember {
                        StaffMemberID = 2,
                        StaffMemberBio = "test",
                        StaffMemberImageLink = "http://LinkOfSomething.com/Link.png",
                        StaffMemberName = "Name",
                        Licesnses = new List<License>
                        {
                            new License {
                                LicenseID = 1,
                                Type = new LicenseType
                                {
                                    LicenseTypeAbbrev = "LTP",
                                    LicenseTypeDescription = "Oregon Licensed Tax Preparer",
                                    LicenseTypeID = 5,
                                    LicenseTypeName = "Licensed Tax Preparer",
                                    LicenseTypeNumberAddendum = "P",
                                 }
                            },
                            new License { LicenseID = 2 }
                        }
                    };
                    */

                    // Null is a legal answer, lets handle an unfound request.
                    if (StaffPerson == null)
                    {
                        return new { success = false };
                    }
                    // Wrap the staff person object in a standard wrapper with a success parameter in its heading.

                    // Jared Notes: We create a new object and wrap a Success variable into it as standard, then any objects we want as data literally get attached
                    // See the StaffMember consuming the StaffPerson object as an example of such things. I left a commented test object override up top for you to
                    // Try it out of you need more examplage. I have turned off lazy loading SO BE CAREFUL WHAT YOU CONSTRUCT!
                    return new { success = true, ID = ID, StaffMember = StaffPerson };
                }catch
                {
                    // Something went wrong, give back a generic failure message.
                    return new { success = false };
                }
            }
        }

        /// <summary>
        /// Get a list of all staff members
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetStaffMembers()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    ICollection<StaffMember> Members = db.Staff.ToList<StaffMember>();

                    if (Members == null)
                    {
                        return "";
                    }
                    return JsonConvert.SerializeObject(Members);
                }
                catch
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Get a list of staff members with a specific license type
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        [Route(Name = "api/Staff/GET/{type}")]
        [HttpGet]
        public string GetStaffMembersByType(string Type)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    LicenseType LType = db.LicenseTypes.Where(c => c.LicenseTypeAbbrev == Type.ToLower()).FirstOrDefault();
                    if (LType == null) { return ""; }
                    ICollection<License> LicenseList = db.Licenses.Where(lt => lt.Type == LType).ToList();
                    ICollection<StaffMember> Members = new List<StaffMember>();
                    foreach(License L in LicenseList)
                    {
                        Members.Add(L.Licensee);
                    }
                    if (Members.Count < 1) { return ""; }
                    return JsonConvert.SerializeObject(Members);
                }
                catch
                {
                    return "";
                }
            }
        }
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}