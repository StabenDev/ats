﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ATS.Models;

namespace ATS.Controllers
{
    [RoutePrefix("api/Price")]
    public class PriceController : ApiController
    {
        /// Get a specific Form by ID number
        /// <param TaxFormFamilyID="ID">ID number of Form Familiy ie 1040, W-2, 1099</param>
        /// <returns>FormFamily</returns> 
        [Route("ID/{id}")]
        [HttpGet]
        public object GetTaxFormFamilyID(int id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
               try
                {
                    int ID;
                    // Grab form from the database if it exists
                    TaxFormFamily TaxForm = db.TaxFormFamilies.FirstOrDefault(c => c.TaxFormFamilyID == id);

                  

                    // Null is a legal answer, lets handle an unfound request.
                    if (TaxForm == null)
                    {
                        return new { success = false };
                    }
                   
                    return new { success = true, ID = id, TaxFormFamily = TaxForm };
                }
                catch
                {
                    // Something went wrong, give back a generic failure message.
                    return new { success = false };
                }
            }
        }

    }
//    [HttpGet]
//    public string Get()
//    {
//        using (ApplicationDbContext db = new ApplicationDbContext())
//        {
//            try
//            {
//                ICollection<TaxFormFamily> Members = db..ToList<StaffMember>();

//                if (Members == null)
//                {
//                    return "";
//                }
//                return JsonConvert.SerializeObject(Members);
//            }
//            catch
//            {
//                return "";
//            }
//        }
//    }
}
