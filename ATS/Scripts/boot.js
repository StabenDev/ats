"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
///<reference path="./../typings/globals/core-js/index.d.ts"/>
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
// Custom Components
var app_component_1 = require("./app.component");
// Route Modules
var home_component_1 = require("./SiteData/Home/home.component");
var pricing_component_1 = require("./SiteData/Home/pricing.component");
var news_component_1 = require("./SiteData/Home/news.component");
var form_component_1 = require("./SiteData/Home/form.component");
var staff_component_1 = require("./SiteData/Home/staff.component");
var contact_component_1 = require("./SiteData/Home/contact.component");
var RouteNotFound_component_1 = require("./SiteData/Error/RouteNotFound.component");
// Routes
var app_routing_1 = require("./app.routing");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            app_routing_1.RoutingRoot
        ],
        declarations: [
            app_component_1.AppComponent,
            home_component_1.HomeComponent,
            pricing_component_1.PricingComponent,
            news_component_1.NewsComponent,
            form_component_1.FormComponent,
            staff_component_1.StaffComponent,
            contact_component_1.ContactComponent,
            RouteNotFound_component_1.RouteNotFoundComponent
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=boot.js.map