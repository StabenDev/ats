"use strict";
// Angular Components
var router_1 = require("@angular/router");
// Custom Components
var home_component_1 = require("./SiteData/Home/home.component");
var pricing_component_1 = require("./SiteData/Home/pricing.component");
var news_component_1 = require("./SiteData/Home/news.component");
var form_component_1 = require("./SiteData/Home/form.component");
var staff_component_1 = require("./SiteData/Home/staff.component");
var contact_component_1 = require("./SiteData/Home/contact.component");
var RouteNotFound_component_1 = require("./SiteData/Error/RouteNotFound.component");
exports.RoutingRoot = router_1.RouterModule.forRoot([
    { path: '', component: home_component_1.HomeComponent },
    { path: 'pricing', component: pricing_component_1.PricingComponent },
    { path: 'news', component: news_component_1.NewsComponent },
    { path: 'form', component: form_component_1.FormComponent },
    { path: 'staff', component: staff_component_1.StaffComponent },
    { path: 'contact', component: contact_component_1.ContactComponent },
    { path: '**', component: RouteNotFound_component_1.RouteNotFoundComponent /* need a 404 component later */ }
]);
//# sourceMappingURL=app.routing.js.map