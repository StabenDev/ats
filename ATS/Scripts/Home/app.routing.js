"use strict";
// Angular Components
var router_1 = require('@angular/router');
// Custom Components
var app_component_1 = require('./app.component');
exports.RoutingRoot = router_1.RouterModule.forRoot([
    { path: '', component: app_component_1.AppComponent },
]);
//# sourceMappingURL=app.routing.js.map